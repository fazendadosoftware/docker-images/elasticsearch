# Elastic search docker image

## Known issues

### ulimit not permitted
run docker command with --previleged flag...

### max virtual memory areas vm.max_map_count [65530] likely too low, increase to at least [262144]
run on the host machine the following command ```bash sysctl -w vm.max_map_count=262144```
To set this value permanently, update the vm.max_map_count setting in /etc/sysctl.conf. To verify after rebooting, run sysctl vm.max_map_count.

Ref: https://github.com/docker-library/docs/pull/734